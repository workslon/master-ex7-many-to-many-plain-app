var React = require('react');
var ReactDOM = require('react-dom');
var BookStore = require('../stores/BookStore');
var PublisherActions = require('../actions/PublisherActions');
var StatusConstants = require('../constants/StatusConstants');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  displayName: 'CreatePublisher',

  componentDidUpdate: function () {
    var notifications = this.props.notifications || {};
    var status = notifications.status;

    if (status === StatusConstants.SUCCESS) {
      var refs = this.refs;
      refs.name.value = refs.email.value = '';
    }
  },

  _createPublisher: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var name = refs.name || {};
    var email = refs.email || {};

    PublisherActions.createPublisher({
      name: name.value,
      email: email.value
    });
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    return (
      <form>
        <h3>Create Publisher</h3>
        <div className="form-group">
          <label forHtml="name">Name</label>
          <input defaultValue="" ref="name" type="text" className="form-control" id="name" placeholder="Name" />
          {errors.name && <span className="text-danger">{errors.name}</span>}
        </div>
        <div className="form-group">
          <label forHtml="email">Email</label>
          <input defaultValue="" ref="email" type="email" className="form-control" id="email" placeholder="Email" />
          {errors.email && <span className="text-danger">{errors.email}</span>}
        </div>
        <button type="submit" onClick={this._createPublisher} className="btn btn-default">Submit</button>
        {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
        {status === StatusConstants.PENDING && <p className="bg-info">Creating...</p>}
        <IndexLink className="back" to="/publishers/list">&laquo; back</IndexLink>
      </form>
    );
  }
});