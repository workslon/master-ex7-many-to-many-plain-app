var React = require('react');
var Link = require('react-router').Link;
var Author = require('./Author.react');

module.exports = React.createClass({
  displayName: 'AuthorList',

  render: function () {
    var notifications = this.props.notifications || {};
    var status = notifications.status;

    return (
      <div>
        <Link className="create-author btn btn-success bt-sm" to="/authors/create">+ Add Author</Link>
        {this.props.authors.length ?
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this.props.authors.map((function (author, i) {
                return (
                  <Author key={i} nr={i + 1} author={author} />
                );
              }).bind(this))}
            </tbody>
          </table>
          : <div>Loading authors...</div>}
      </div>
    );
  }
});