var React = require('react');
var BookActions = require('../actions/BookActions');
var BookStore = require('../stores/BookStore');
var IndexLink = require('react-router').IndexLink;
var StatusConstants = require('../constants/StatusConstants');
var PublisherStore = require('../stores/PublisherStore');
var AuthorStore = require('../stores/AuthorStore');

module.exports = React.createClass({
  displayName: 'UpdateBook',

  componentWillMount: function() {
    this.book = BookStore.getBook(this.props.params.id);
  },

  _updateBook: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var isbn = refs.isbn || {};
    var title = refs.title || {};
    var year = refs.year || {};
    var publisher = refs.publisher || {};
    var authors = refs.authors || {};
    var selectedAuthors = [].slice.call(authors.selectedOptions).map(function (option) {
      return {
        objectId: option.value
      }
    });

    BookActions.updateBook(this.book, {
      title: title.value,
      year: parseInt(year.value),
      publisher: {
        objectId: publisher.value
      },
      authors: selectedAuthors
    });
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;
    var publishers = PublisherStore.getAllPublishers();
    var authors = AuthorStore.getAllAuthors();
    var selectedAuthors = this.book && Array.isArray(this.book.authors) ? this.book.authors.map(function (author) {
      return author.objectId;
    }) : [];

    return (
        <div>
          <h3>Update Book</h3>
          {this.book ?
            <form>
              <div className="form-group">
                <label forHtml="isbn">ISBN</label>
                <input defaultValue={this.book.isbn} disabled="disabled" ref="isbn" type="text" className="form-control" id="isbn" placeholder="ISBN" />
              </div>
              <div className="form-group">
                <label forHtml="title">Title</label>
                <input defaultValue={this.book.title} ref="title" type="text" className="form-control" id="title" placeholder="Title" />
                {errors.title && <span className="text-danger">{errors.title}</span>}
              </div>
              <div className="form-group">
                <label forHtml="year">Year</label>
                <input defaultValue={this.book.year} ref="year" type="text" className="form-control" id="year" placeholder="Year" />
                {errors.year && <span className="text-danger">{errors.year}</span>}
              </div>
              <div className="form-group">
                <label forHtml="publisher">Publishers</label>
                <select defaultValue={this.book.publisher ? this.book.publisher.objectId : ''} className="form-control" ref="publisher">
                  <option value="">-No Publisher-</option>
                  {publishers.map(function (publisher) {
                    return <option value={publisher.objectId} key={publisher.objectId}>{publisher.name} - {publisher.email}</option>
                  })}
                </select>
              </div>
              <div className="form-group">
                <label forHtml="year">Authors</label>
                <select defaultValue={selectedAuthors} className="form-control" ref="authors" multiple>
                  {authors.map(function (author) {
                    return <option value={author.objectId} key={author.objectId}>{author.id} - {author.name}</option>
                  })}
                </select>
              </div>
              <button type="submit" onClick={this._updateBook} className="btn btn-default">Submit</button>
              {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
              {status === StatusConstants.PENDING && <p className="bg-info">Updating...</p>}
              <IndexLink className="back" to="/">&laquo; back</IndexLink>
            </form>
          : <div>No book found...</div>}
        </div>
    );
  }
});