var React = require('react');
var Link = require('react-router').Link;
var AuthorActions = require('../actions/AuthorActions');

module.exports = React.createClass({
  displayName: 'Author',

  _deleteAuthor: function () {
    AuthorActions.deleteAuthor(this.props.author);
  },

  render: function () {
    var author = this.props.author,
        updatePath = '/authors/update/' + author.objectId;

    return (
      <tr>
        <td scope="row">{this.props.nr}</td>
        <td>{author.id}</td>
        <td>{author.name}</td>
        <td className="action-links">
          <Link className="btn btn-primary btn-xs" to={updatePath}>Update</Link>
          <a className="btn btn-danger btn-xs" onClick={this._deleteAuthor}>Delete</a>
        </td>
      </tr>
    );
  }
});