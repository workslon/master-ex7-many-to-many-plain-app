var React = require('react');
var AuthorActions = require('../actions/AuthorActions');
var AuthorStore = require('../stores/AuthorStore');
var StatusConstants = require('../constants/StatusConstants');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  displayName: 'UpdateAuthor',

  componentWillMount: function() {
    this.author = AuthorStore.getAuthor(this.props.params.id);
  },

  _updateAuthor: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var name = refs.name || {};

    AuthorActions.updateAuthor(this.author, {
      name: name.value
    });
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    return (
        <div>
          <h3>Update Author</h3>
          {this.author ?
            <form>
              <div className="form-group">
                <label forHtml="id">ID</label>
                <input defaultValue={this.author.authorId} disabled="disabled" ref="id" type="text" className="form-control" id="id" placeholder="ID" />
              </div>
              <div className="form-group">
                <label forHtml="name">Name</label>
                <input defaultValue={this.author.name} ref="name" type="name" className="form-control" id="name" placeholder="Name" />
                {errors.name && <span className="text-danger">{errors.name}</span>}
              </div>
              <button type="submit" onClick={this._updateAuthor} className="btn btn-default">Submit</button>
              {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
              {status === StatusConstants.PENDING && <p className="bg-info">Updating...</p>}
              <IndexLink className="back" to="/authors/list">&laquo; back</IndexLink>
            </form>
          : <div>No author found...</div>}
        </div>
    );
  }
});