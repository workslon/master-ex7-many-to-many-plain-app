var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

module.exports = {
  clearNotifications: function () {
    AppDispatcher.dispatch({
      type: AppConstants.CLEAR_NOTIFICATIONS
    });
  }
};