var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var validator = require('validator');
var XHR = require('xhr-promise');
var xhr = new XHR();

xhr.defaults = {
  url: 'https://public-library-api.herokuapp.com/api/classes/Author/',
  headers: {
    'X-Parse-Application-Id': 'MyH7zW1'
  }
};

module.exports = {
  getAuthors: function () {
    var promise = xhr.GET();

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_AUTHORS,
      success: AppConstants.REQUEST_AUTHORS_SUCCESS,
      failure: AppConstants.REQUEST_AUTHORS_ERROR
    });
  },

  createAuthor: function (data) {
    validator.validate(data);

    if (validator.isValid()) {
      xhr.GET({
        url: xhr.defaults.url + '?where={"authorId":"' + data.authorId + '"}'
      })
      .then(function (result) {
          try {
            if (JSON.parse(result).results.length) {
              AppDispatcher.dispatch({
                type: 'NON_UNIQUE_ID'
              });
            } else {
              AppDispatcher.dispatchAsync(xhr.POST({data: data}), {
                request: AppConstants.REQUEST_AUTHOR_SAVE,
                success: AppConstants.AUTHOR_SAVE_SUCCESS,
                failure: AppConstants.AUTHOR_SAVE_ERROR
              }, data);
            }
          } catch(e) {}
        },

        function (error) {
          alert('Server error!');
        }
      );
    } else {
      AppDispatcher.dispatch({
        type: AppConstants.AUTHOR_VALIDATION_ERROR,
        errors: validator.errors
      });
    }
  },

  updateAuthor: function (author, newData) {
    var promise;

    validator.validate(newData);

    if (validator.isValid()) {
      promise = xhr.PUT({
        url: xhr.defaults.url + author.objectId,
        data: newData
      });

      newData.objectId = author.objectId;

      AppDispatcher.dispatchAsync(promise, {
        request: AppConstants.REQUEST_AUTHOR_UPDATE,
        success: AppConstants.AUTHOR_UPDATE_SUCCESS,
        failure: AppConstants.AUTHOR_UPDATE_ERROR
      }, newData);
    } else {
      AppDispatcher.dispatch({
        type: 'AUTHOR_VALIDATION_ERROR',
        errors: validator.errors
      });
    }
  },

  deleteAuthor: function (author) {
    var promise = xhr.DELETE({
      url: xhr.defaults.url + author.objectId
    });

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_AUTHOR_DESTROY,
      success: AppConstants.AUTHOR_DESTROY_SUCCESS,
      failure: AppConstants.AUTHOR_DESTROY_ERROR
    }, author)
  }
};