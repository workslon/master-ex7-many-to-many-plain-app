var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var validator = require('validator');
var XHR = require('xhr-promise');
var xhr = new XHR();

xhr.defaults = {
  url: 'https://public-library-api.herokuapp.com/api/classes/Book/',
  headers: {
    'X-Parse-Application-Id': 'MyH7zW1'
  }
};

module.exports = {
  getBooks: function () {
    AppDispatcher.dispatch({
      type: AppConstants.REQUEST_BOOKS
    });

    xhr.GET({
      url: 'https://public-library-api.herokuapp.com/api/classes/Book/?include=publisher',
    }).then(function (books) {
      books = JSON.parse(books).results;
      var promises = books.map(function (book) {
        var params = '?where=' + JSON.stringify({
          $relatedTo: {
            object: {
              __type: 'Pointer',
              className: 'Book',
              objectId: book.objectId
            },
            key: 'authors'
          }
        });

        return xhr.GET({
          url: 'https://public-library-api.herokuapp.com/api/classes/Author/' + params
        });
      });

      Promise.all(promises).then(function (responses) {
        responses.forEach(function (response, i) {
          books[i].authors = JSON.parse(response).results;
        });

        AppDispatcher.dispatch({
          type: AppConstants.REQUEST_BOOKS_SUCCESS,
          result: books
        });
      }, function (error) {
        AppDispatcher.dispatch({
          type: AppConstants.REQUEST_BOOKS_ERROR,
          error: error
        });
      });
    }, function (error) {
      AppDispatcher.dispatch({
        type: AppConstants.REQUEST_BOOKS_ERROR,
        error: error
      });
    });
  },

  createBook: function (data) {
    validator.validate(data);

    if (validator.isValid()) {
      xhr.GET({
        url: xhr.defaults.url + '?where={"isbn":"' + data.isbn + '"}'
      })
      .then(function (result) {
          try {
            if (JSON.parse(result).results.length) {
              AppDispatcher.dispatch({
                type: AppConstants.NON_UNIQUE_ISBN
              });
            } else {
              // publisher
              if (data.publisher && data.publisher.objectId) {
                data.publisher.__type = 'Pointer';
                data.publisher.className = 'Publisher';
              } else {
                data.publisher = undefined;
              }

              // authors
              if (data.authors && data.authors.length) {
                var authors = {
                  __op: 'AddRelation',
                  objects: data.authors.map(function (author) {
                    return {
                      __type: 'Pointer',
                      className: 'Author',
                      objectId: author.objectId
                    }
                  })
                };

                data.authors = authors;
              } else {
                data.authors = undefined;
              }

              AppDispatcher.dispatchAsync(xhr.POST({data: data}), {
                request: AppConstants.REQUEST_BOOK_SAVE,
                success: AppConstants.BOOK_SAVE_SUCCESS,
                failure: AppConstants.BOOK_SAVE_ERROR
              }, data);
            }
          } catch(e) {}
        },

        function (error) {
          alert('Server error!');
        }
      );
    } else {
      AppDispatcher.dispatch({
        type: AppConstants.BOOK_VALIDATION_ERROR,
        errors: validator.errors
      });
    }
  },

  updateBook: function (book, newData) {
    var promise;

    validator.validate(newData);

    if (validator.isValid()) {
      AppDispatcher.dispatch({
        type: AppConstants.REQUEST_BOOK_UPDATE
      });
      // publisher
      if (newData.publisher && newData.publisher.objectId) {
        newData.publisher.__type = 'Pointer';
        newData.publisher.className = 'Publisher';
      } else {
        newData.publisher = {
          __op: 'Delete'
        }
      }

      // authors
      var authors = {};
      var newAuthors = newData.authors;

      // remove old pointers to `authors` if the `authors` have been added or updated
      if (newData.authors && newData.authors.length) {
        if (book.authors && book.authors.length) {
          authors = {
            __op: 'RemoveRelation',
            objects: book.authors.map(function (author) {
              return {
                __type: 'Pointer',
                className: 'Author',
                objectId: author.objectId
              }
            })
          };

          newData.authors = authors;
        } else {
          authors = {
            __op: 'AddRelation',
            objects: newData.authors.map(function (author) {
              return {
                __type: 'Pointer',
                className: 'Author',
                objectId: author.objectId
              }
            })
          };

          newData.authors = authors;
        }

        if (authors.__op === 'RemoveRelation') {
          promise = xhr.PUT({
            url: xhr.defaults.url + book.objectId,
            data: newData
          });

          promise
            .then(function (response) {
              newData.authors = {
                __op: 'AddRelation',
                objects: newAuthors.map(function (author) {
                  return {
                    __type: 'Pointer',
                    className: 'Author',
                    objectId: author.objectId
                  }
                })
              };

              promise = xhr.PUT({
                url: xhr.defaults.url + book.objectId,
                data: newData
              });

              newData.objectId = book.objectId;

              AppDispatcher.dispatchAsync(promise, {
                request: AppConstants.REQUEST_BOOK_UPDATE,
                success: AppConstants.BOOK_UPDATE_SUCCESS,
                failure: AppConstants.BOOK_UPDATE_ERROR
              }, newData);
            }, function (error) {
              throw new Error(error);
            });
        } else {
          promise = xhr.PUT({
            url: xhr.defaults.url + book.objectId,
            data: newData
          });

          newData.objectId = book.objectId;

          AppDispatcher.dispatchAsync(promise, {
            request: AppConstants.REQUEST_BOOK_UPDATE,
            success: AppConstants.BOOK_UPDATE_SUCCESS,
            failure: AppConstants.BOOK_UPDATE_ERROR
          }, newData);
        }
      } else {
        promise = xhr.PUT({
          url: xhr.defaults.url + book.objectId,
          data: newData
        });

        newData.objectId = book.objectId;

        AppDispatcher.dispatchAsync(promise, {
          request: AppConstants.REQUEST_BOOK_UPDATE,
          success: AppConstants.BOOK_UPDATE_SUCCESS,
          failure: AppConstants.BOOK_UPDATE_ERROR
        }, newData);
      }
    } else {
      AppDispatcher.dispatch({
        type: AppConstants.BOOK_VALIDATION_ERROR,
        errors: validator.errors
      });
    }
  },

  deleteBook: function (book) {
    AppDispatcher.dispatch({
      type: AppConstants.REQUEST_BOOK_DESTROY
    });

    book.publisher = {
      __op: 'Delete'
    };

    if (book.authors && book.authors.length) {
      var authors = {
        __op: 'RemoveRelation',
        objects: book.authors.map(function (author) {
          return {
            __type: 'Pointer',
            className: 'Author',
            objectId: author.objectId
          }
        })
      };

      book.authors = authors;
    }

    xhr.PUT({
      url: xhr.defaults.url + book.objectId,
      data: book
    }).then(function (response) {
      var promise = xhr.DELETE({
        url: xhr.defaults.url + book.objectId
      });

      AppDispatcher.dispatchAsync(promise, {
        request: AppConstants.REQUEST_BOOK_DESTROY,
        success: AppConstants.BOOK_DESTROY_SUCCESS,
        failure: AppConstants.BOOK_DESTROY_ERROR
      }, book)
    }, function (error) {
      throw new Error(error);
    });
  }
};